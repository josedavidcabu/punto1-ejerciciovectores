/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.Objects;

/**
 *
 * @author madar
 */
public class Estudiante {

    private int codigo;
    private String nombre;
    private String email;

    /**
     * crea un estudiante con parametros
     *
     * @param codigo un entero con el codigo del estudiante
     * @param nombre una cadena con el codigo del estudiante
     * @param email una cadena con el email del estudiante
     */
    public Estudiante(int codigo, String nombre, String email) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.email = email;
    }

    /**
     * crea un estudiante sin parametros
     */
    public Estudiante() {
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + this.codigo;
        hash = 47 * hash + Objects.hashCode(this.email);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        boolean eq = false;
        if (this == obj) {
            eq = true;
        }
        if (obj == null) {
            eq = false;
        }
        if (getClass() != obj.getClass()) {
            eq = false;
        }
        final Estudiante other = (Estudiante) obj;
        if (this.codigo == other.codigo) {
            eq = true;
        }
        return eq;
    }

}
