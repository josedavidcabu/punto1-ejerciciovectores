/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.Arrays;
import java.util.Objects;

/**
 *
 * @author madar
 */
public class Materia {
    
    private int codMateria;
    private String nombreMateria;
    private Estudiante myEstudiantes[];
/**
 * crea una materia sin parametros
 */
    public Materia() {
    }
/**.
 * crea una materia con parametros
 * @param codMateria un entero con el codigo de la materia
 * @param nombreMateria una cadena con el nombre de la materia
 * @param myEstudiantes  un arreglo de estudiantes que cursan la materia
 */
    public Materia(int codMateria, String nombreMateria, Estudiante[] myEstudiantes) {
        this.codMateria = codMateria;
        this.nombreMateria = nombreMateria;
        this.myEstudiantes = myEstudiantes;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + this.codMateria;
        hash = 97 * hash + Objects.hashCode(this.nombreMateria);
        hash = 97 * hash + Arrays.deepHashCode(this.myEstudiantes);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Materia other = (Materia) obj;
        if (this.codMateria != other.codMateria) {
            return false;
        }
        return true;
    }

    

   
  
    
}
